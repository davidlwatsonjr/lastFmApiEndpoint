<?php

require_once('../vendor/autoload.php');
include_once('../config.php');

use Slim\Slim;
use Dandelionmood\LastFm\LastFm;

$app = new Slim();

$app->response->headers->set('Access-Control-Allow-Origin', 'https://davidlwatsonjr.github.io');
$app->response->headers->set('Access-Control-Allow-Headers', 'X-Requested-With');
$app->response->headers->set('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');

$app->response->headers->set('Content-Type', 'application/json');

$app->options('/:uri', function() {});
$app->options('/:uri/:uri2', function() {});

session_start();

$lastFmSessionKey = (isset($_SESSION['lastfm.session.key'])) ? $_SESSION['lastfm.session.key'] : null;

$lastFmApiKey = isset($config['lastFmApiKey']) ? $config['lastFmApiKey'] : '';
$lastFmApiSecret = isset($config['lastFmApiSecret']) ? $config['lastFmApiSecret'] : '';
$lastfm = new LastFm( $lastFmApiKey , $lastFmApiSecret, $lastFmSessionKey);

/**
 * First page to call, it will redirect the user to Last.fm to
 * let him authorize the application.
*/
$app->get('/auth/connect', function() use ($app, $lastfm) {
  // We need to compute a callback URL that will be called when the user
  // allows the application.
  $callback_url = $lastfm->auth_get_url(
    $app->request()->getUrl()
      .$app->urlFor('auth_callback')
  );

  $app->redirect( $callback_url );
});

/**
  * Second page that will be called by Last.fm. The token to get a session
  * will be given through a GET variable.
*/
$app->get('/auth/callback', function() use ($app, $lastfm, $config) {
  $token = $app->request()->get('token');

  $responseData = [];

  try {
    // We try to get a session using the token we're given
    $lastFmResponse = $lastfm->auth_get_session( $token );

    $_SESSION['lastfm.session.key']= $lastFmResponse->session->key;
    unset($lastFmResponse->session->key);

    $_SESSION['lastfm.session.info'] = $lastFmResponse->session;

    $responseData['lastFmResponse'] = $lastFmResponse;
    $responseData['lastFmSessionInfo'] = $_SESSION['lastfm.session.info'];
    $responseData['success'] = true;
  } catch( Exception $e ) {
    $responseData['errorMessage'] = $e->getMessage();
    $responseData['success'] = false;
  }

  $app->redirect($config['authRedirectUrl']);
})->name('auth_callback');

$app->get('/auth/session', function() {
  $responseData = [];

  if (isset($_SESSION['lastfm.session.info'])) {
    $responseData['lastFmSessionInfo'] = $_SESSION['lastfm.session.info'];
  }

  echo json_encode($responseData);
})->name('auth_session');

$app->get('/:method', function($method) use ($app, $lastfm) {
  $responseData = [];

  try {
    $lastFmResponse = $lastfm->$method($app->request->get(), true);

    $responseData['lastFmResponse'] = $lastFmResponse;
    $responseData['success'] = true;
  } catch (Exception $e) {
    $responseData['errorMessage'] = $e->getMessage();
    $responseData['success'] = false;
  }

  if (isset($_SESSION['lastfm.session.info'])) {
    $responseData['lastFmSessionInfo'] = $_SESSION['lastfm.session.info'];
  }

  echo json_encode($responseData);
})->name('defaultGet');

$app->post('/:method', function($method) use ($app, $lastfm) {
  $responseData = [];

  try {
    $lastFmResponse = $lastfm->$method($app->request->post(), true);

    $responseData['lastFmResponse'] = $lastFmResponse;
    $responseData['success'] = true;
  } catch (Exception $e) {
    $responseData['errorMessage'] = $e->getMessage();
    $responseData['success'] = false;
  }

  if (isset($_SESSION['lastfm.session.info'])) {
    $responseData['lastFmSessionInfo'] = $_SESSION['lastfm.session.info'];
  }

  echo json_encode($responseData);
})->name('defaultPost');

$app->run();

